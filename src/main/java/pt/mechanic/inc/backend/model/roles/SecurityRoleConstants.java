package pt.mechanic.inc.backend.model.roles;

public class SecurityRoleConstants {
    public static final String ROLE_SUPER = "ROLE_SUPER";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String ROLE_SELLER = "ROLE_SELLER";

}
