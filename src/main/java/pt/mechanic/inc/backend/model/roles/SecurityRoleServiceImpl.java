package pt.mechanic.inc.backend.model.roles;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.mechanic.inc.backend.config.security.models.SecurityProperties;

import java.util.HashMap;
import java.util.Map;

@Service
public class SecurityRoleServiceImpl implements SecurityRoleService {

	private static final Logger log = LoggerFactory.getLogger(SecurityRoleService.class);
	private final FirebaseAuth firebaseAuth;

	private final SecurityProperties securityProps;

	public SecurityRoleServiceImpl(FirebaseAuth firebaseAuth, SecurityProperties securityProps) {
		this.firebaseAuth = firebaseAuth;
		this.securityProps = securityProps;
	}

	@Override
	public void addRole(String uid, String role) throws Exception {
		try {
			UserRecord user = firebaseAuth.getUser(uid);
			Map<String, Object> claims = new HashMap<>();
			user.getCustomClaims().forEach((k, v) -> claims.put(k, v));
			if (securityProps.getValidApplicationRoles().contains(role)) {
				if (!claims.containsKey(role)) {
					claims.put(role.toLowerCase(), true);
				}
				firebaseAuth.setCustomUserClaims(uid, claims);
			} else {
				throw new Exception("Not a valid Application role, Allowed roles => "
						+ securityProps.getValidApplicationRoles().toString());
			}

		} catch (FirebaseAuthException e) {
			log.error("Firebase Auth Error ", e);
		}

	}

	@Override
	public void removeRole(String uid, String role) {
		try {
			UserRecord user = firebaseAuth.getUser(uid);
			Map<String, Object> claims = new HashMap<>();
			user.getCustomClaims().forEach((k, v) -> claims.put(k, v));
			if (claims.containsKey(role)) {
				claims.remove(role);
			}
			firebaseAuth.setCustomUserClaims(uid, claims);
		} catch (FirebaseAuthException e) {
			log.error("Firebase Auth Error ", e);
		}
	}

}
