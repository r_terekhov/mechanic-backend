package pt.mechanic.inc.backend.config.security;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pt.mechanic.inc.backend.config.security.models.Credentials;
import pt.mechanic.inc.backend.config.security.models.FirebaseUser;
import pt.mechanic.inc.backend.config.security.models.SecurityProperties;
import pt.mechanic.inc.backend.model.roles.SecurityRoleConstants;
import pt.mechanic.inc.backend.model.roles.SecurityRoleService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SecurityFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(SecurityFilter.class);

    private final SecurityService securityService;

    private final SecurityProperties securityProps;

    private final SecurityRoleService securityRoleService;

    private final FirebaseAuth firebaseAuth;

    public SecurityFilter(SecurityService securityService, SecurityProperties securityProps, SecurityRoleService securityRoleService, FirebaseAuth firebaseAuth) {
        this.securityService = securityService;
        this.securityProps = securityProps;
        this.securityRoleService = securityRoleService;
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        authorize(request);
        filterChain.doFilter(request, response);
    }

    private void authorize(HttpServletRequest request) {
        String sessionCookieValue = null;
        FirebaseToken decodedToken = null;
        Credentials.CredentialType type = null;
        // Token verification
        boolean strictServerSessionEnabled = securityProps.getFirebaseProps().isEnableStrictServerSession();
        String token = securityService.getBearerToken(request);
        try {
            if (!strictServerSessionEnabled && token != null && !token.equals("null")
                    && !token.equalsIgnoreCase("undefined")) {
                decodedToken = firebaseAuth.verifyIdToken(token);
                type = Credentials.CredentialType.ID_TOKEN;
            }
        } catch (FirebaseAuthException e) {
            log.error("Firebase Exception:: {}", e.getLocalizedMessage());
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        FirebaseUser firebaseUser = firebaseTokenToUserDto(decodedToken);
        // Handle roles
        if (firebaseUser != null) {
            // Handle Super Role
            if (securityProps.getSuperAdmins().contains(firebaseUser.getEmail())) {
                if (!decodedToken.getClaims().containsKey(SecurityRoleConstants.ROLE_SUPER)) {
                    try {
                        securityRoleService.addRole(decodedToken.getUid(), SecurityRoleConstants.ROLE_SUPER);
                    } catch (Exception e) {
                        log.error("Super Role registeration expcetion ", e);
                    }
                }
                authorities.add(new SimpleGrantedAuthority(SecurityRoleConstants.ROLE_SUPER));
            }
            // Handle Other roles
            decodedToken.getClaims().forEach((k, v) -> authorities.add(new SimpleGrantedAuthority(k)));
            // Set security context
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(firebaseUser,
                    new Credentials(type, decodedToken, token, sessionCookieValue), authorities);
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    private FirebaseUser firebaseTokenToUserDto(FirebaseToken decodedToken) {
        FirebaseUser firebaseUser = null;
        if (decodedToken != null) {
            firebaseUser = new FirebaseUser();
            firebaseUser.setUid(decodedToken.getUid());
            firebaseUser.setName(decodedToken.getName());
            firebaseUser.setEmail(decodedToken.getEmail());
            firebaseUser.setPicture(decodedToken.getPicture());
            firebaseUser.setIssuer(decodedToken.getIssuer());
            firebaseUser.setEmailVerified(decodedToken.isEmailVerified());
        }
        return firebaseUser;
    }

}
