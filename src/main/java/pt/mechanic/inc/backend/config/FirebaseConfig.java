package pt.mechanic.inc.backend.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import pt.mechanic.inc.backend.config.security.models.SecurityProperties;

import java.io.IOException;

@Configuration
public class FirebaseConfig {


	private final SecurityProperties secProps;

	public FirebaseConfig(SecurityProperties secProps) {
		this.secProps = secProps;
	}

	@Primary
	@Bean
	public FirebaseApp getfirebaseApp() throws IOException {
		FirebaseOptions options = FirebaseOptions.builder().setCredentials(GoogleCredentials.getApplicationDefault())
				.setDatabaseUrl(secProps.getFirebaseProps().getDatabaseUrl()).build();
		if (FirebaseApp.getApps().isEmpty()) {
			FirebaseApp.initializeApp(options);
		}
		return FirebaseApp.getInstance();
	}

	@Bean
	public FirebaseAuth getAuth() throws IOException {
		return FirebaseAuth.getInstance(getfirebaseApp());
	}
}
