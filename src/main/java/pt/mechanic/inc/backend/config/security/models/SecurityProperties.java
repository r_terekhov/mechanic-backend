package pt.mechanic.inc.backend.config.security.models;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("security")
@Data
public class SecurityProperties {
	private FirebaseProperties firebaseProps;
	List<String> superAdmins;
	List<String> validApplicationRoles;

}
