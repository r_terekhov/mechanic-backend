package pt.mechanic.inc.backend.config.security.models;

import lombok.Data;

@Data
public class FirebaseProperties {
    private String databaseUrl;
    private boolean enableStrictServerSession;
}
