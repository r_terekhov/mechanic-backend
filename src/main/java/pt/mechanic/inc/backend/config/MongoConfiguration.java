/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * vk.com/r_terekhov
 * Copyright (c) 2020.
 */

package pt.mechanic.inc.backend.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = {"pt.mechanic.inc.backend.repository"})
@EnableAutoConfiguration
@Slf4j
public class MongoConfiguration {

}

