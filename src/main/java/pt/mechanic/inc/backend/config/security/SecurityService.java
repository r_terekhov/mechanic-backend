package pt.mechanic.inc.backend.config.security;


import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pt.mechanic.inc.backend.config.security.models.Credentials;
import pt.mechanic.inc.backend.config.security.models.FirebaseUser;

import javax.servlet.http.HttpServletRequest;

@Service
public class SecurityService {

	public FirebaseUser getFirebaseUser() {
		FirebaseUser firebaseUserPrincipal = null;
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Object principal = securityContext.getAuthentication().getPrincipal();
		if (principal instanceof FirebaseUser) {
			firebaseUserPrincipal = ((FirebaseUser) principal);
		}
		return firebaseUserPrincipal;
	}

	public Credentials getCredentials() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		return (Credentials) securityContext.getAuthentication().getCredentials();
	}

	public String getBearerToken(HttpServletRequest request) {
		String bearerToken = null;
		String authorization = request.getHeader("Authorization");
		if (StringUtils.hasText(authorization) && authorization.startsWith("Bearer ")) {
			bearerToken = authorization.substring(7);
		}
		return bearerToken;
	}

}
