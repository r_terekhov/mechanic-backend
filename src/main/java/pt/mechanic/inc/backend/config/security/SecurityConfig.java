package pt.mechanic.inc.backend.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pt.mechanic.inc.backend.config.security.models.SecurityProperties;
import pt.mechanic.inc.backend.controller.error.model.ErrorCode;
import pt.mechanic.inc.backend.dto.ApiResponseWithErrorCode;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


	private final ObjectMapper objectMapper;

	private final SecurityProperties restSecProps;

	private final SecurityFilter tokenAuthenticationFilter;

	public SecurityConfig(ObjectMapper objectMapper, SecurityProperties restSecProps, SecurityFilter tokenAuthenticationFilter) {
		this.objectMapper = objectMapper;
		this.restSecProps = restSecProps;
		this.tokenAuthenticationFilter = tokenAuthenticationFilter;
	}


	@Bean
	public AuthenticationEntryPoint restAuthenticationEntryPoint() {
		return (httpServletRequest, httpServletResponse, e) -> {
			ApiResponseWithErrorCode<Object> error = ApiResponseWithErrorCode.error(ErrorCode.ACCESS_DENIED_EXCEPTION.getCode(), "access denied", e.getLocalizedMessage());
			int errorCode = 401;
			httpServletResponse.setContentType("application/json;charset=UTF-8");
			httpServletResponse.setStatus(errorCode);
			httpServletResponse.getWriter().write(objectMapper.writeValueAsString(error));
		};
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().formLogin().disable()
				.httpBasic().disable().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint())
				.and().authorizeRequests()
				.antMatchers(
						"/public/*",
						"/auth/isUserExists",
						"/auth/register",
						"/request/create")
				.permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll().anyRequest().authenticated().and()
				.addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
