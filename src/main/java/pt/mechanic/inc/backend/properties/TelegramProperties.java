package pt.mechanic.inc.backend.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties(prefix = "telegram")
@Configuration
public class TelegramProperties {
    private String botToken;
    private Integer chatId;
}
