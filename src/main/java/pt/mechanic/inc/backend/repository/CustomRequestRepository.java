package pt.mechanic.inc.backend.repository;

import pt.mechanic.inc.backend.domain.Request;

import java.util.Map;

public interface CustomRequestRepository {
    Request updateStatus(String requestId, Map<String, Object> parameters, Request.ProcessingEvent event);
}
