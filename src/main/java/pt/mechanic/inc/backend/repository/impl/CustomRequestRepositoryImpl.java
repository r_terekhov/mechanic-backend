package pt.mechanic.inc.backend.repository.impl;

import org.springframework.data.mongodb.core.MongoOperations;
import pt.mechanic.inc.backend.domain.Request;
import pt.mechanic.inc.backend.repository.CustomRequestRepository;

import java.util.Map;

public class CustomRequestRepositoryImpl implements CustomRequestRepository {

    private final MongoOperations operation;

    public CustomRequestRepositoryImpl(MongoOperations operation) {
        this.operation = operation;
    }

    @Override
    public Request updateStatus(String requestId, Map<String, Object> parameters, Request.ProcessingEvent event) {
        //todo
        return null;
    }
}
