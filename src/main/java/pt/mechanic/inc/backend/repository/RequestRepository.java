package pt.mechanic.inc.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.mechanic.inc.backend.domain.Request;

import java.util.List;

@Repository
public interface RequestRepository extends MongoRepository<Request, String>, CustomRequestRepository {

    List<Request> findRequestsByOrderByCreatedDesc();
}
