package pt.mechanic.inc.backend.service;

import pt.mechanic.inc.backend.dto.CreateRequestDto;
import pt.mechanic.inc.backend.dto.RequestDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

public interface RequestService {

    RequestDto createRequest(@Valid CreateRequestDto createRequestDto);

    RequestDto closeRequest(@NotBlank String requestId, @NotBlank String closeComment);

    RequestDto markInWork(@NotBlank String requestId);

    List<RequestDto> getRequests();
}
