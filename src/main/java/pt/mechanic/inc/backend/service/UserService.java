package pt.mechanic.inc.backend.service;

import com.google.firebase.auth.FirebaseAuthException;
import pt.mechanic.inc.backend.domain.User;
import pt.mechanic.inc.backend.dto.RegisterUserDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public interface UserService {

    boolean isUserExistsByEmail(@NotBlank String email);

    User register(@NotBlank String token, @Valid RegisterUserDto registerUserDto) throws FirebaseAuthException;

    User getUserFromAuthContext(@NotBlank String uid);
}
