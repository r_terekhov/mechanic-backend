package pt.mechanic.inc.backend.service.mock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.mechanic.inc.backend.service.RequestNotifierService;

@Service
@Profile("local")
public class TelegramBotMock implements RequestNotifierService {
    private static final Logger log = LoggerFactory.getLogger(RequestNotifierService.class);

    @Override
    public void sendMessage(String message) {
        log.info("message {} send successfully!", message);
    }
}
