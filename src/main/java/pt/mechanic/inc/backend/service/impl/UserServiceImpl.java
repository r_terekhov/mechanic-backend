package pt.mechanic.inc.backend.service.impl;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.mechanic.inc.backend.config.security.SecurityService;
import pt.mechanic.inc.backend.domain.User;
import pt.mechanic.inc.backend.dto.RegisterUserDto;
import pt.mechanic.inc.backend.repository.UserRepository;
import pt.mechanic.inc.backend.service.UserService;
import pt.mechanic.inc.backend.utils.FirebaseUtils;

import javax.validation.constraints.NotBlank;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final FirebaseUtils firebaseUtils;

    private final SecurityService securityService;

    public UserServiceImpl(UserRepository userRepository, FirebaseUtils firebaseUtils, SecurityService securityService) {
        this.userRepository = userRepository;
        this.firebaseUtils = firebaseUtils;
        this.securityService = securityService;
    }

    @Override
    public boolean isUserExistsByEmail(@NotBlank String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public User register(String token, RegisterUserDto registerUserDto) throws FirebaseAuthException {
        FirebaseToken firebaseTokenFromToken = firebaseUtils.getFirebaseTokenFromToken(token);
        User user = userRepository.save(new User(registerUserDto.getEmail(), firebaseTokenFromToken.getUid()));
        return user;
    }

    @Override
    public User getUserFromAuthContext(@NotBlank String uid) {
        return userRepository.findByUid(securityService.getFirebaseUser().getUid()).get();
    }


}
