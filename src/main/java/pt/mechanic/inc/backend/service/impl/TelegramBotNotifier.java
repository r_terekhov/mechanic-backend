package pt.mechanic.inc.backend.service.impl;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.mechanic.inc.backend.properties.TelegramProperties;
import pt.mechanic.inc.backend.service.RequestNotifierService;

@Service
@Primary
@Profile("!local")
public class TelegramBotNotifier implements RequestNotifierService {
    private static final Logger log = LoggerFactory.getLogger(TelegramBotNotifier.class);
    private final TelegramProperties telegramProperties;
    private final TelegramBot bot;

    public TelegramBotNotifier(TelegramProperties telegramProperties) {
        this.telegramProperties = telegramProperties;
        bot = new TelegramBot(telegramProperties.getBotToken());
    }

    @Override
    public void sendMessage(String message) {
        SendResponse response = bot.execute(new SendMessage(telegramProperties.getChatId(), message).parseMode(ParseMode.HTML));
//        bot.execute(new SendMessage(telegramProperties.getChatId(), message).parseMode(ParseMode.HTML), new Callback() {
//            @Override
//            public void onResponse(BaseRequest request, BaseResponse response) {
//                log.info("notification send successfully");
//            }
//            @Override
//            public void onFailure(BaseRequest request, IOException e) {
//                log.error(e.getMessage());
//            }
//        });
    }
}
