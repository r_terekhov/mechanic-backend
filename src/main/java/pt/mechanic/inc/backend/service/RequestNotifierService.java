package pt.mechanic.inc.backend.service;

public interface RequestNotifierService {

    void sendMessage(String message);
}
