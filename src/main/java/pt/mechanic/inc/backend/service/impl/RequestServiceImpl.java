package pt.mechanic.inc.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.mechanic.inc.backend.config.security.SecurityService;
import pt.mechanic.inc.backend.controller.error.model.exceptions.RequestNotFoundException;
import pt.mechanic.inc.backend.domain.Request;
import pt.mechanic.inc.backend.dto.CreateRequestDto;
import pt.mechanic.inc.backend.dto.RequestDto;
import pt.mechanic.inc.backend.repository.RequestRepository;
import pt.mechanic.inc.backend.service.RequestNotifierService;
import pt.mechanic.inc.backend.service.RequestService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class RequestServiceImpl implements RequestService {
    private static final Logger log = LoggerFactory.getLogger(RequestService.class);

    private final RequestRepository requestRepository;

    private final RequestNotifierService requestNotifierService;

    private final SecurityService securityService;

    public RequestServiceImpl(RequestRepository requestRepository, RequestNotifierService requestNotifierService, SecurityService securityService) {
        this.requestRepository = requestRepository;
        this.requestNotifierService = requestNotifierService;
        this.securityService = securityService;
    }

    @Override
    public RequestDto createRequest(@Valid CreateRequestDto createRequestDto) {
        Request request = requestRepository.save(Request.open(createRequestDto));
        requestNotifierService.sendMessage(RequestDto.messageOnOpenedRequestEvent(request));
        return RequestDto.of(request);
    }

    @Override
    public RequestDto markInWork(@NotBlank String requestId) {
        Request request = requestRepository.findById(requestId).orElseThrow(() -> new RequestNotFoundException(format("request with id %s not found", requestId)));
        String user = securityService.getFirebaseUser().getEmail();
        Request inWorkRequest = requestRepository.save(request.inWork(user));
        requestNotifierService.sendMessage(RequestDto.messageOnInWorkRequestEvent(inWorkRequest));
        return RequestDto.of(request);
    }

    @Override
    public List<RequestDto> getRequests() {
        return requestRepository.findRequestsByOrderByCreatedDesc().stream().map(RequestDto::of).collect(Collectors.toList());
    }

    @Override
    public RequestDto closeRequest(@NotBlank String requestId, @NotBlank String closeComment) {
        Request request = requestRepository.findById(requestId).orElseThrow(() -> new RequestNotFoundException(format("request with id %s not found", requestId)));
        String user = securityService.getFirebaseUser().getEmail();
        Request closedRequest = requestRepository.save(request.close(closeComment, user));
        requestNotifierService.sendMessage(RequestDto.messageOnCloseRequestEvent(closedRequest));
        return RequestDto.of(request);
    }


}
