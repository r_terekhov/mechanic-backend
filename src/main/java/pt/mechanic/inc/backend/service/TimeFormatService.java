package pt.mechanic.inc.backend.service;

import lombok.Data;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

import static java.lang.String.format;

public interface TimeFormatService {
    String DATE_FORMAT = "dd.MM.yy";
    String DATE_FULL_YEAR_FORMAT = "dd.MM.yyyy";
    String TIME_FORMAT = "HH:mm";
    String DATE_TIME_FORMAT = "dd.MM.yy HH:mm";
    String DATE_FULL_YEAR_TIME_FORMAT = "dd.MM.yyyy HH:mm";

    DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    DateTimeFormatter DATE_FULL_YEAR_FORMATTER = DateTimeFormatter.ofPattern(DATE_FULL_YEAR_FORMAT);
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    DateTimeFormatter DATE_FULL_YEAR_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FULL_YEAR_TIME_FORMAT);
    DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_FORMAT);

    static String formattedDateTimeFromSeconds(long timestampSecondsSinceEpoch, String zoneOffset) {
        ZonedDateTime performedInUtc = ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestampSecondsSinceEpoch), ZoneId.of("UTC"));
        ZonedDateTime performedInZone = performedInUtc
                .withZoneSameInstant(ZoneId.ofOffset("UTC", ZoneOffset.of(zoneOffset)));
        return DATE_TIME_FORMATTER.format(performedInZone);
    }

    static String formattedDateTimeFromMillis(long timestampMillisSinceEpoch, String zoneOffset) {
        ZonedDateTime performedInUtc = ZonedDateTime.ofInstant(Instant.ofEpochMilli(timestampMillisSinceEpoch), ZoneId.of("UTC"));
        ZonedDateTime performedInZone = performedInUtc
                .withZoneSameInstant(ZoneId.ofOffset("UTC", ZoneOffset.of(zoneOffset)));
        return DATE_TIME_FORMATTER.format(performedInZone);
    }

    static ZonedDateTime zonedDateTimeFrom(String formattedDateTime, String zoneOffset) {
        DateTimeFormatter formatter;
        if (formattedDateTime.length() == 14) {
            formatter = DATE_TIME_FORMATTER;
        } else if (formattedDateTime.length() == 16) {
            formatter = DATE_FULL_YEAR_TIME_FORMATTER;
        } else {
            throw new IllegalArgumentException(
                    format("illegal formatted date-time: %s; dd.MM.yy HH:mm and dd.MM.yyyy HH:mm are allowed",
                            formattedDateTime)
            );
        }
        return LocalDateTime.parse(formattedDateTime, formatter).atZone(ZoneId.ofOffset("UTC", ZoneOffset.of(zoneOffset)));
    }

    Function<String, LocalTime> TIME_FROM_STRING = (str) -> LocalTime.parse(str, TIME_FORMATTER);

    static boolean nowAtOffsetZoneInTimePeriod(String startTime, String endTime, String zoneOffset) {
        LocalTime start = TimeFormatService.TIME_FROM_STRING.apply(startTime);
        LocalTime end = TimeFormatService.TIME_FROM_STRING.apply(endTime);

        OffsetTime offsetTimeStart = start.atOffset(ZoneOffset.of(zoneOffset));
        OffsetTime offsetTimeEnd = end.atOffset(ZoneOffset.of(zoneOffset));

        OffsetTime nowAtZone = OffsetTime.now(ZoneId.ofOffset("UTC", ZoneOffset.of(zoneOffset)));
        return (nowAtZone.isAfter(offsetTimeStart) || nowAtZone.isEqual(offsetTimeStart)) &&
                nowAtZone.isBefore(offsetTimeEnd);
    }

    static boolean nowAtOffsetZoneInDateTimePeriod(String date, String startTime, String endTime, String zoneOffset) {
        LocalDate periodDate;
        if (date.length() == 8) {
            periodDate = LocalDate.parse(date, DATE_FORMATTER);
        } else if (date.length() == 10) {
            periodDate = LocalDate.parse(date, DATE_FULL_YEAR_FORMATTER);
        } else {
            throw new IllegalArgumentException(format("unexpected date string length: %s;" +
                            "dd.mm.yy and dd.mm.yyyy formats allowed",
                    date));
        }

        LocalTime start = TimeFormatService.TIME_FROM_STRING.apply(startTime);
        LocalTime end = TimeFormatService.TIME_FROM_STRING.apply(endTime);

        LocalDateTime startLocalDateTime = periodDate.atTime(start);
        LocalDateTime endLocalDateTime = periodDate.atTime(end);

        OffsetDateTime offsetDateTimeStart = startLocalDateTime.atOffset(ZoneOffset.of(zoneOffset));
        OffsetDateTime offsetDateTimeEnd = endLocalDateTime.atOffset(ZoneOffset.of(zoneOffset));

        OffsetDateTime nowAtZone = OffsetDateTime.now(ZoneId.ofOffset("UTC", ZoneOffset.of(zoneOffset)));
        return (nowAtZone.isAfter(offsetDateTimeStart) || nowAtZone.isEqual(offsetDateTimeStart)) &&
                nowAtZone.isBefore(offsetDateTimeEnd);
    }

    static long nowSecondsUtc() {
        return ZonedDateTime.now(ZoneId.of("UTC")).toEpochSecond();
    }

    static TimestampAndFormatted nowSecondsUtcDateTimeFormatted() {
        long seconds = nowSecondsUtc();
        return new TimestampAndFormatted(seconds, formattedDateTimeFromSeconds(seconds, "+0"));
    }

    static long nowMillisUtc() {
        return ZonedDateTime.now(ZoneId.of("UTC")).toInstant().toEpochMilli();
    }

    static TimestampAndFormatted nowMillisUtcDateTimeFormatted() {
        long millis = nowMillisUtc();
        return new TimestampAndFormatted(millis, formattedDateTimeFromMillis(millis, "+0"));
    }

    @Data
    final class TimestampAndFormatted {
        private final long timestamp;
        private final String formatted;
    }
}
