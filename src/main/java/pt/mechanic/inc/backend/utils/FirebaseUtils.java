package pt.mechanic.inc.backend.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Component
public class FirebaseUtils {

    private final FirebaseAuth firebaseAuth;

    public FirebaseUtils(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    public FirebaseToken getFirebaseTokenFromToken(@NotBlank String token) throws FirebaseAuthException {
        return firebaseAuth.verifyIdToken(token);
    }
}
