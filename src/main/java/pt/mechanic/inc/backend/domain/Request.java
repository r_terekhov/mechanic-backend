package pt.mechanic.inc.backend.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import pt.mechanic.inc.backend.dto.CreateRequestDto;
import pt.mechanic.inc.backend.service.TimeFormatService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Request extends Document {

    private String clientName;

    private String clientPhone;

    private boolean needDiagnostic;


    private long arrivalMechanicDateTime;

    private String address;

    private String machine;

    private int machineYear;

    private String problemDescription;

    @Indexed
    private Status status;

    private List<ProcessingEvent> processingEvents = new ArrayList<>();

    public Request(String clientName, String clientPhone, boolean needDiagnostic, long arrivalMechanicDateTime, String address, String machine, int machineYear, String problemDescription, Status status) {
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.needDiagnostic = needDiagnostic;
        this.arrivalMechanicDateTime = arrivalMechanicDateTime;
        this.address = address;
        this.machine = machine;
        this.machineYear = machineYear;
        this.problemDescription = problemDescription;
        this.status = status;
    }

    public enum Status {
        OPEN,
        IN_WORK,
        CLOSED
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class ProcessingEvent {
        private long timestampSecUtc;
        private String dateTimeFormatted;
        private String author;
        private String event;
        private EventType eventType;
        private Map<String, Object> parameters;


        public enum EventType {
            REQUEST_CREATED,
            REQUEST_MARKED_IN_WORK,
            REQUEST_CLOSE,
        }
    }


    public static Request open(CreateRequestDto createRequestDto) {
        return new Request(
                createRequestDto.getClientName(),
                createRequestDto.getClientPhone(),
                createRequestDto.isNeedDiagnostic(),
                createRequestDto.getArrivalMechanicDateTime(),
                createRequestDto.getAddress(),
                createRequestDto.getMachine(),
                createRequestDto.getMachineYear(),
                createRequestDto.getProblemDescription(),
                Status.OPEN,
                new ArrayList<>(Collections.singleton(new ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "CLIENT",
                        "Request created by client",
                        ProcessingEvent.EventType.REQUEST_CREATED,
                        null
                )))
        );
    }

    public Request inWork(String initiator) {
        this.setStatus(Status.IN_WORK);
        this.processingEvents.add(new ProcessingEvent(
                TimeFormatService.nowSecondsUtc(),
                TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                "SUPER_ADMIN",
                format("Request marked in work by super admin %s", initiator),
                ProcessingEvent.EventType.REQUEST_MARKED_IN_WORK,
                null));
        return this;
    }

    public Request close(String closedComment, String initiator) {
        this.setStatus(Status.CLOSED);
        this.processingEvents.add(new ProcessingEvent(
                TimeFormatService.nowSecondsUtc(),
                TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                "SUPER_ADMIN",
                format("Request marked closed by super admin %s", initiator),
                ProcessingEvent.EventType.REQUEST_CLOSE,
                Map.of("closeComment", closedComment)));
        return this;
    }
}
