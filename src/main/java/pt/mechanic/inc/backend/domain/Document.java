package pt.mechanic.inc.backend.domain;

import lombok.Getter;
import org.springframework.data.annotation.Id;

import java.lang.reflect.Field;
import java.time.Instant;

@org.springframework.data.mongodb.core.mapping.Document
public abstract class Document {
    @Id
    @Getter
    private String id;

    @Getter
    private long created;


    public Document() {
        this.created = Instant.now().getEpochSecond();
    }


    public static void setId(Document document, String id) {
        try {

            Field declaredField = Document.class.getDeclaredField("id");

            declaredField.setAccessible(true);
            declaredField.set(document, id);
            declaredField.setAccessible(false);

        } catch (NoSuchFieldException
                | SecurityException
                | IllegalArgumentException
                | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
