package pt.mechanic.inc.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MechanicBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MechanicBackendApplication.class, args);
    }

}
