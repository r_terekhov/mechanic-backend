package pt.mechanic.inc.backend.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.mechanic.inc.backend.config.security.SecurityService;

@RestController
@RequestMapping("protected")
public class ProtectedController {


	private final SecurityService securityService;

	public ProtectedController(SecurityService securityService) {
		this.securityService = securityService;
	}

	@GetMapping("data")
	public String getProtectedData() {
		String name = securityService.getFirebaseUser().getName();
		return name.split("\\s+")[0] + ", you have accessed protected data from spring boot";
	}

}
