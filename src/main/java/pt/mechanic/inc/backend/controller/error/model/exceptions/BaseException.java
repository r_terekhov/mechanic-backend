package pt.mechanic.inc.backend.controller.error.model.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * base exception for creating custom exceptions
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

}
