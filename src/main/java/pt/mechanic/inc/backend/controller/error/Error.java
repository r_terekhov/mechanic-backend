package pt.mechanic.inc.backend.controller.error;

/**
 * interface for giving info about exception: code and base string error description
 */
public interface Error {

    Integer getCode();

    String getBaseMessage();
}
