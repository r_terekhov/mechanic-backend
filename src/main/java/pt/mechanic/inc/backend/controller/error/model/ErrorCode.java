package pt.mechanic.inc.backend.controller.error.model;

public enum ErrorCode {
    EXCEPTION(0),
    RUNTIME_EXCEPTION(1),
    BASE_EXCEPTION(2),
    ACCESS_DENIED_EXCEPTION(3),
    REQUEST_NOT_FOUND_EXCEPTION(4);
    private int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
