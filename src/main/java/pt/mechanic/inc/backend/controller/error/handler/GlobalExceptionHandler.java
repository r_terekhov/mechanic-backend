package pt.mechanic.inc.backend.controller.error.handler;

import com.google.firebase.auth.FirebaseAuthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import pt.mechanic.inc.backend.controller.error.model.ErrorCode;
import pt.mechanic.inc.backend.controller.error.model.exceptions.BaseException;
import pt.mechanic.inc.backend.controller.error.model.exceptions.RequestNotFoundException;
import pt.mechanic.inc.backend.dto.ApiResponseWithErrorCode;

/**
 * class for handle exceptions, which weren't handled on controller level
 */
@SuppressWarnings("rawtypes")
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler(value = RequestNotFoundException.class)
    public ResponseEntity<ApiResponseWithErrorCode> handleRequestNotFoundException(RequestNotFoundException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(ApiResponseWithErrorCode.error(ErrorCode.REQUEST_NOT_FOUND_EXCEPTION.getCode(), "REQUEST_NOT_FOUND_EXCEPTION", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = FirebaseAuthException.class)
    public ResponseEntity<ApiResponseWithErrorCode> handleFirebaseAuthException(FirebaseAuthException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(ApiResponseWithErrorCode.error(ErrorCode.ACCESS_DENIED_EXCEPTION.getCode(), "Firebase auth exception", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ApiResponseWithErrorCode> handleBaseException(BaseException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(ApiResponseWithErrorCode.error(ErrorCode.BASE_EXCEPTION.getCode(), "Base exception occurred", e.getMessage()), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<ApiResponseWithErrorCode> handleRuntimeException(RuntimeException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(ApiResponseWithErrorCode.error(ErrorCode.RUNTIME_EXCEPTION.getCode(), "Runtime exception occurred", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiResponseWithErrorCode> handleException(Exception e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(ApiResponseWithErrorCode.error(ErrorCode.EXCEPTION.getCode(), "Global exception occurred", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}