package pt.mechanic.inc.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.mechanic.inc.backend.dto.ApiResponseWithErrorCode;
import pt.mechanic.inc.backend.dto.CreateRequestDto;
import pt.mechanic.inc.backend.dto.RequestDto;
import pt.mechanic.inc.backend.model.roles.IsSuper;
import pt.mechanic.inc.backend.service.RequestService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@Validated
@RequestMapping("request")
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @PostMapping("create")
    public ResponseEntity<ApiResponseWithErrorCode<RequestDto>> create(
            @Valid @RequestBody CreateRequestDto createRequestDto) {
        return new ResponseEntity<>(ApiResponseWithErrorCode.response(requestService.createRequest(createRequestDto)), HttpStatus.OK);
    }

    @GetMapping("getAll")
    @IsSuper
    public ResponseEntity<ApiResponseWithErrorCode<List<RequestDto>>> getAll() {
        return new ResponseEntity<>(ApiResponseWithErrorCode.response(requestService.getRequests()), HttpStatus.OK);
    }

    @PostMapping("inWork")
    @IsSuper
    public ResponseEntity<ApiResponseWithErrorCode<RequestDto>> markInWork(
            @RequestParam @NotBlank String requestId) {
        return new ResponseEntity<>(ApiResponseWithErrorCode.response(requestService.markInWork(requestId)), HttpStatus.OK);
    }

    @PostMapping("close")
    @IsSuper
    public ResponseEntity<ApiResponseWithErrorCode<RequestDto>> close(
            @RequestParam @NotBlank String requestId,
            @RequestParam @NotBlank String closeComment) {
        return new ResponseEntity<>(ApiResponseWithErrorCode.response(requestService.closeRequest(requestId, closeComment)), HttpStatus.OK);
    }
}
