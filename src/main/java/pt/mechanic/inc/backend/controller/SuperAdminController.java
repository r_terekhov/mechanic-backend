package pt.mechanic.inc.backend.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import org.springframework.web.bind.annotation.*;
import pt.mechanic.inc.backend.config.security.SecurityService;
import pt.mechanic.inc.backend.model.roles.IsSuper;
import pt.mechanic.inc.backend.model.roles.SecurityRoleService;

@RestController
@RequestMapping("super")
public class SuperAdminController {


	private final SecurityRoleService securityRoleService;


	private final SecurityService securityService;


	private final FirebaseAuth firebaseAuth;

	public SuperAdminController(SecurityRoleService securityRoleService, SecurityService securityService, FirebaseAuth firebaseAuth) {
		this.securityRoleService = securityRoleService;
		this.securityService = securityService;
		this.firebaseAuth = firebaseAuth;
	}

	@GetMapping("user")
	@IsSuper
	public UserRecord getUser(@RequestParam String email) throws Exception {
		return firebaseAuth.getUserByEmail(email);
	}

	@PutMapping("role/add")
	@IsSuper
	public void addRole(@RequestParam String uid, @RequestParam String role) throws Exception {
		securityRoleService.addRole(uid, role);
	}

	@DeleteMapping("role/remove")
	@IsSuper
	public void removeRole(@RequestParam String uid, @RequestParam String role) {
		securityRoleService.removeRole(uid, role);

	}

	@GetMapping("data")
	@IsSuper
	public String getSuperData() {
		String name = securityService.getFirebaseUser().getName();
		return name.split("\\s+")[0] + ", you have accessed super data from spring boot";
	}

}
