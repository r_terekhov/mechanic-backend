package pt.mechanic.inc.backend.controller.auth;

import com.google.firebase.auth.FirebaseAuthException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.mechanic.inc.backend.config.security.SecurityService;
import pt.mechanic.inc.backend.domain.User;
import pt.mechanic.inc.backend.dto.ApiResponseWithErrorCode;
import pt.mechanic.inc.backend.dto.RegisterUserDto;
import pt.mechanic.inc.backend.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("auth")
@Validated
public class AuthController {

	private final SecurityService securityService;

	private final UserService userService;

	public AuthController(SecurityService securityService, UserService userService) {
		this.securityService = securityService;
		this.userService = userService;
	}

	@GetMapping("isUserExists")
	public ResponseEntity<ApiResponseWithErrorCode<Boolean>> isUserExists(
			@RequestParam String email) {
		return new ResponseEntity<>(ApiResponseWithErrorCode.response(userService.isUserExistsByEmail(email)), HttpStatus.OK);
	}

	@PostMapping("register")
	public ResponseEntity<ApiResponseWithErrorCode<User>> register(
			@RequestHeader String token,
			@Valid @RequestBody RegisterUserDto registerUserDto) throws FirebaseAuthException {
		User user = userService.register(token, registerUserDto);
		return new ResponseEntity<>(ApiResponseWithErrorCode.response(user), HttpStatus.OK);
	}


//	@PostMapping("me")
//	public FirebaseUser getUser() {
//		return securityService.getUser();
//	}
//
//	@GetMapping("create/token")
//	public String getCustomToken() throws FirebaseAuthException {
//		return FirebaseAuth.getInstance().createCustomToken(String.valueOf(securityService.getUser().getUid()));
//	}

}
