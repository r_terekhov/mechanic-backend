package pt.mechanic.inc.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param <T> response class
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@NoArgsConstructor
public class ApiResponseWithErrorCode<T> {
    /**
     * response
     */
    private T response;
    /**
     * error code
     */
    private Integer code;
    /**
     * base error description
     */
    private String baseError;
    /**
     * additional info about error
     */
    private String comment;

    private ApiResponseWithErrorCode(T response, Integer code, String baseError, String comment) {
        this.response = response;
        this.code = code;
        this.baseError = baseError;
        this.comment = comment;
    }

    public static <T> ApiResponseWithErrorCode<T> response(T response) {
        return new ApiResponseWithErrorCode<>(response, null, null, null);
    }

    public static <T> ApiResponseWithErrorCode<T> error(Integer code, String baseError, String comment) {
        return new ApiResponseWithErrorCode<>(null, code, baseError, comment);
    }
}
