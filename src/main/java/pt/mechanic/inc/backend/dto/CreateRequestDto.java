package pt.mechanic.inc.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateRequestDto {

    private String clientName;

    private String clientPhone;

    private boolean needDiagnostic;

    private long arrivalMechanicDateTime;

    private String address;

    private String machine;

    private int machineYear;

    private String problemDescription;
}
