package pt.mechanic.inc.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pt.mechanic.inc.backend.domain.Request;
import pt.mechanic.inc.backend.service.TimeFormatService;

import java.util.List;

import static java.lang.String.format;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Data
@AllArgsConstructor
public class RequestDto extends BaseDto {

    private String clientName;

    private String clientPhone;

    private boolean needDiagnostic;

    private long arrivalMechanicDateTime;

    private String address;

    private String machine;

    private int machineYear;

    private String problemDescription;

    private String status;

    private List<Request.ProcessingEvent> processingEvents;


    public static RequestDto of(Request request) {
        return RequestDto.builder()
                .id(request.getId())
                .createdTimestamp(request.getCreated())
                .clientName(request.getClientName())
                .clientPhone(request.getClientPhone())
                .needDiagnostic(request.isNeedDiagnostic())
                .arrivalMechanicDateTime(request.getArrivalMechanicDateTime())
                .address(request.getAddress())
                .machine(request.getMachine())
                .machineYear(request.getMachineYear())
                .problemDescription(request.getProblemDescription())
                .status(request.getStatus().toString())
                .processingEvents(request.getProcessingEvents())
                .build();
    }

    public static String messageOnOpenedRequestEvent(Request request) {
        return format("<b> \uD83C\uDD95 New request with id %s</b>\n", request.getId()) +
                format("<b>Address</b> is %s\n", request.getAddress()) +
                format("<b>Client's name</b> is %s\n", request.getClientName()) +
                format("<b>Client's phone</b> is %s\n", request.getClientPhone()) +
                format("<b>Client need diagnostic</b> is %s\n", request.isNeedDiagnostic()) +
                format("<b>Client's car</b> is %s\n", request.getMachine()) +
                format("<b>Client's car year</b> is %s\n", request.getMachineYear()) +
                format("<b>Client's problem</b> is %s\n", request.getProblemDescription()) +
                format("<b>Timestamp in UTC</b> %s\n", TimeFormatService.formattedDateTimeFromSeconds(request.getCreated(), "+0"));
    }

    public static String messageOnInWorkRequestEvent(Request request) {
        return format("<b>\uD83E\uDDD1\u200D\uD83D\uDD27 Request with id %s in progress</b>\n", request.getId()) +
                format("<b>Address</b> is %s\n", request.getAddress()) +
                format("<b>Client's name</b> is %s\n", request.getClientName()) +
                format("<b>Client's phone</b> is %s\n", request.getClientPhone()) +
                format("<b>Timestamp in UTC</b> %s\n", TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted());
    }

    public static String messageOnCloseRequestEvent(Request request) {
        return format("<b>✅ Request with id %s in done</b>\n", request.getId()) +
                format("<b>Address</b> is %s\n", request.getAddress()) +
                format("<b>Client's name</b> is %s\n", request.getClientName()) +
                format("<b>Client's phone</b> is %s\n", request.getClientPhone()) +
                format("<b>Timestamp in UTC</b> %s\n", TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted());
    }
}
