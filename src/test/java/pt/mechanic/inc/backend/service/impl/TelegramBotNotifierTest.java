package pt.mechanic.inc.backend.service.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import pt.mechanic.inc.backend.domain.Request;
import pt.mechanic.inc.backend.dto.RequestDto;
import pt.mechanic.inc.backend.properties.TelegramProperties;

import java.time.Instant;

@Ignore
public class TelegramBotNotifierTest {

    private TelegramProperties telegramProperties;

    private TelegramBotNotifier telegramBotNotifier;

    @Before
    public void setUp() {
        telegramProperties = new TelegramProperties();
        telegramProperties.setBotToken("token");
        telegramProperties.setChatId(123);
        telegramBotNotifier = new TelegramBotNotifier(telegramProperties);
    }

    @Test
    public void sendMessage() {
        Request request = new Request(
                "clientName",
                "clientPhone",
                true,
                Instant.now().getEpochSecond(),
                "address",
                "car",
                2007,
                "problem",
                Request.Status.CLOSED
        );
        String message = RequestDto.messageOnOpenedRequestEvent(request);
        String message2 = RequestDto.messageOnInWorkRequestEvent(request);
        String message3 = RequestDto.messageOnCloseRequestEvent(request);
        telegramBotNotifier.sendMessage(message);
        telegramBotNotifier.sendMessage(message2);
        telegramBotNotifier.sendMessage(message3);
    }
}