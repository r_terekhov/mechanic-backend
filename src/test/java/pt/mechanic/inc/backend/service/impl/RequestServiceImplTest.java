package pt.mechanic.inc.backend.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;
import pt.mechanic.inc.backend.config.security.SecurityService;
import pt.mechanic.inc.backend.config.security.models.FirebaseUser;
import pt.mechanic.inc.backend.domain.Document;
import pt.mechanic.inc.backend.domain.Request;
import pt.mechanic.inc.backend.dto.CreateRequestDto;
import pt.mechanic.inc.backend.dto.RequestDto;
import pt.mechanic.inc.backend.repository.RequestRepository;
import pt.mechanic.inc.backend.service.TimeFormatService;
import pt.mechanic.inc.backend.service.mock.TelegramBotMock;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("local")
public class RequestServiceImplTest {

    @Mock
    RequestRepository requestRepository;

    @Mock
    TelegramBotMock telegramBotMock;

    @Mock
    SecurityService securityService;

    @InjectMocks
    RequestServiceImpl requestService;


    @Test
    public void createRequest() {
        Request.ProcessingEvent event = new Request.ProcessingEvent(
                TimeFormatService.nowSecondsUtc(),
                TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                "CLIENT",
                "Request created by client",
                Request.ProcessingEvent.EventType.REQUEST_CREATED,
                null
        );

        Request request = new Request(
                "clientName",
                "clientPhone",
                true,
                Instant.now().getEpochSecond(),
                "address",
                "car",
                2007,
                "problem",
                Request.Status.OPEN,
                Collections.singletonList(event)
        );

        Document.setId(request, "id");

        CreateRequestDto createRequestDto = new CreateRequestDto(
                "clientName",
                "clientPhone",
                true,
                Instant.now().getEpochSecond(),
                "address",
                "car",
                2007,
                "problem"
        );

//        Gson gson = new Gson();
//        System.out.println(gson.toJson(createRequestDto));

        when(requestRepository.save(any())).thenReturn(request);
        RequestDto openedRequest = requestService.createRequest(createRequestDto);
        System.out.println(openedRequest);
        assertEquals(RequestDto.of(request), openedRequest);

    }

    @Test
    public void markInWork() {
        ArrayList<Request.ProcessingEvent> processingEvents = new ArrayList<>(List.of(
                new Request.ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "CLIENT",
                        "Request created by client",
                        Request.ProcessingEvent.EventType.REQUEST_CREATED,
                        null
                ),
                new Request.ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "SUPER_ADMIN",
                        "Request marked in work by super admin",
                        Request.ProcessingEvent.EventType.REQUEST_MARKED_IN_WORK,
                        null)
        ));
        Request request = new Request(
                "clientName",
                "clientPhone",
                true,
                Instant.now().getEpochSecond(),
                "address",
                "car",
                2007,
                "problem",
                Request.Status.IN_WORK,
                processingEvents
        );

        Document.setId(request, "id");
        when(securityService.getFirebaseUser()).thenReturn(new FirebaseUser("test_user"));
        when(requestRepository.findById(any())).thenReturn(java.util.Optional.of(request));
        when(requestRepository.save(any())).thenReturn(request);
        RequestDto inWorkRequest = requestService.markInWork("id");
        assertEquals(RequestDto.of(request), inWorkRequest);
    }

    @Test
    public void closeRequest() {
        String closedComment = "closedComment";
        ArrayList<Request.ProcessingEvent> processingEvents = new ArrayList<>(List.of(
                new Request.ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "CLIENT",
                        "Request created by client",
                        Request.ProcessingEvent.EventType.REQUEST_CREATED,
                        null
                ),
                new Request.ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "SUPER_ADMIN",
                        "Request marked in work by super admin",
                        Request.ProcessingEvent.EventType.REQUEST_MARKED_IN_WORK,
                        null),

                new Request.ProcessingEvent(
                        TimeFormatService.nowSecondsUtc(),
                        TimeFormatService.nowSecondsUtcDateTimeFormatted().getFormatted(),
                        "SUPER_ADMIN",
                        "Request marked closed by super admin",
                        Request.ProcessingEvent.EventType.REQUEST_CLOSE,
                        Map.of("closeComment", closedComment))
        ));
        Request request = new Request(
                "clientName",
                "clientPhone",
                true,
                Instant.now().getEpochSecond(),
                "address",
                "car",
                2007,
                "problem",
                Request.Status.CLOSED
        );

        Document.setId(request, "id");
        when(securityService.getFirebaseUser()).thenReturn(new FirebaseUser("test_user"));
        when(requestRepository.findById(any())).thenReturn(java.util.Optional.of(request));
        when(requestRepository.save(any())).thenReturn(request);
        RequestDto closedRequest = requestService.closeRequest("id", closedComment);
        assertEquals(RequestDto.of(request), closedRequest);
    }


}